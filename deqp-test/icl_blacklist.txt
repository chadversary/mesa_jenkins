dEQP-EGL.functional.buffer_age.no_preserve
dEQP-EGL.functional.query_context.simple.query_api
dEQP-EGL.functional.robustness.reset_context.fixed_function_pipeline.reset_status.index_buffer_out_of_bounds
dEQP-GLES31.functional.synchronization.inter_call.without_memory_barrier.ssbo_atomic_counter_mixed_dispatch_100_calls_1k_invocations

# GPU hang on ICL
dEQP-GLES31.functional.compute.basic.image_atomic_op_local_size_1
dEQP-GLES31.functional.compute.basic.image_atomic_op_local_size_8
dEQP-GLES31.functional.image_load_store.2d.qualifiers
dEQP-GLES31.functional.image_load_store.2d_array.qualifiers
dEQP-GLES31.functional.image_load_store.3d.qualifiers
dEQP-GLES31.functional.image_load_store.cube.qualifiers
dEQP-GLES31.functional.shaders.multisample_interpolation.interpolate_at_sample
dEQP-GLES31.functional.synchronization.inter_invocation
dEQP-GLES31.functional.ssbo.atomic.compswap
dEQP-GLES31.functional.tessellation.shader_input_output.barrier

# flaky on ICL
dEQP-EGL.functional.multithread.window
dEQP-GLES2.functional.shaders.random.all_features.fragment.16
dEQP-GLES3.functional.fbo.blit.depth_stencil.stencil_index8_scale
dEQP-GLES3.functional.shaders.random.all_features.fragment.16
dEQP-GLES3.functional.texture.mipmap.2d
dEQP-GLES3.functional.texture.mipmap.3d
dEQP-GLES3.functional.texture.mipmap.cube
dEQP-GLES31.functional.debug.negative_coverage.callbacks.compute.exceed_atomic_counters_limit
dEQP-GLES31.functional.image_load_store.buffer.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.buffer.atomic.comp_swap_r32ui_return_value
dEQP-GLES31.functional.sample_shading.min_sample_shading.multisample_renderbuffer_samples_8_color
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.isampler2d
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.isampler2darray
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.sampler2d
dEQP-GLES31.functional.shaders.opaque_type_indexing.sampler.uniform.compute.sampler3d
dEQP-GLES31.functional.tessellation.invariance.primitive_set.quads_fractional_even_spacing_ccw_point_mode
